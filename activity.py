# Activity:
# 1. Create an abstract class called Animal that has the following abstract methods:
#   - eat(food)
#   - make_sound()
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
#   - Properties:
#     - Name
#     - Breed
#     - Age
#   - Methods:
#     - Getters
#     - Setters
#     - Implementation of abstract methods
#     - call()

from abc import ABC, abstractclassmethod

class Animal(ABC):

	@abstractclassmethod
	def eat(self):
		pass

	@abstractclassmethod
	def make_sound(self):
		pass

class Cat(Animal):

	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age


	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age


	def get_name(self):
		print(f"This cat's name is {self._name}") 

	def get_breed(self):
		print(f"This cat's breed is {self._breed}") 

	def get_age(self):
		print(f"This cat's age is {self._age}") 


	def eat(self, food):
		self._food = food
		print(f'Serve me {self._food}')

	def make_sound(self):
		self._sound = 'Miaow! Nyaw! Nyaaaaa!'
		print(f'{self._sound}')

	def call(self):
		print(f'{self._name}, come on!')

class Dog(Animal):

	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age


	def set_name(self, name):
		self._name = name

	def set_breed(self, breed):
		self._breed = breed

	def set_age(self, age):
		self._age = age


	def get_name(self):
		print(f"This dog's name is {self._name}") 

	def get_breed(self):
		print(f"This dog's breed is {self._breed}") 

	def get_age(self):
		print(f"This dog's age is {self._age}") 


	def eat(self, food):
		self._food = food
		print(f'Serve me {self._food}')

	def make_sound(self):
		self._sound = 'Bark! Woof! Arf!'
		print(f'{self._sound}')

	def call(self):
		print(f'{self._name}, come on!')

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
